{
  "$schema": "http://json-schema.org/draft-07/schema#",
  "title": "PrettyConfig",
  "description": "This is mainly to generate a pretty and accurate config JSON SCHEMA",
  "type": "object",
  "properties": {
    "alpha": {
      "description": "Optional alpha value",
      "type": [
        "integer",
        "null"
      ],
      "format": "uint8",
      "minimum": 0.0
    },
    "backend": {
      "description": "Which backend to use, see backends.rs",
      "anyOf": [
        {
          "$ref": "#/definitions/Backend"
        },
        {
          "type": "null"
        }
      ]
    },
    "check_contrast": {
      "description": "This flags ensures good contrast between images, by doing some w3m calculations. However it isn't required and should only be turn on when you notice bad contrast between many images.",
      "type": [
        "boolean",
        "null"
      ]
    },
    "color_space": {
      "description": "Which colorspace to use, see colorspaces.rs",
      "anyOf": [
        {
          "$ref": "#/definitions/ColorSpace"
        },
        {
          "type": "null"
        }
      ]
    },
    "env_vars": {
      "description": "Enables the use of enviromental variables in the targets template paths",
      "type": [
        "boolean",
        "null"
      ]
    },
    "fallback_generator": {
      "description": "How to 'generate' colors when there aren't enough colors to create the `palette`. This appears as \"Artificially generating colors..\" in cli",
      "anyOf": [
        {
          "$ref": "#/definitions/FallbackGenerator"
        },
        {
          "type": "null"
        }
      ]
    },
    "palette": {
      "description": "Which palette to use, see palettes.rs",
      "anyOf": [
        {
          "$ref": "#/definitions/Palette"
        },
        {
          "type": "null"
        }
      ]
    },
    "saturation": {
      "description": "Maybe the user requires more vivid colors",
      "type": [
        "integer",
        "null"
      ],
      "format": "uint8",
      "minimum": 0.0
    },
    "templates": {
      "description": "The [templates] header, here you can define multiple templates",
      "type": [
        "object",
        "null"
      ],
      "additionalProperties": {
        "$ref": "#/definitions/Fields"
      }
    },
    "threshold": {
      "description": "What threshold to use to differentiate colors, if not defined, wallust automatically looks for the best fit",
      "default": null,
      "type": [
        "integer",
        "null"
      ],
      "format": "uint8",
      "minimum": 0.0
    }
  },
  "definitions": {
    "Backend": {
      "description": "This indicates what 'parser' method to use, defined in the config file. Corresponds to the modules inside this module",
      "oneOf": [
        {
          "description": "Read and return the whole image pixels (more precision, slower)",
          "type": "string",
          "enum": [
            "full"
          ]
        },
        {
          "description": "Resizes the image before parsing, mantaining it's aspect ratio",
          "type": "string",
          "enum": [
            "resized"
          ]
        },
        {
          "description": "Uses image magick `convert` to generate the colors, like pywal",
          "type": "string",
          "enum": [
            "wal"
          ]
        },
        {
          "description": "Faster algo hardcoded to 512x512 (no ratio respected)",
          "type": "string",
          "enum": [
            "thumb"
          ]
        },
        {
          "description": "A much faster resize algo that uses SIMD. For some reason it fails on some images where `resized` doesn't, for this reason it doesn't *replace* but rather it's a new option.",
          "type": "string",
          "enum": [
            "fastresize"
          ]
        },
        {
          "description": "Kmeans is an algo that divides and picks pixels all around the image, giving a more diverse look.",
          "type": "string",
          "enum": [
            "kmeans"
          ]
        }
      ]
    },
    "ColorSpace": {
      "description": "Corresponds to the modules inside this module and `color_space` parameter in the config file.",
      "oneOf": [
        {
          "description": "Uses Cie L*a*b color space",
          "type": "string",
          "enum": [
            "lab"
          ]
        },
        {
          "description": "Variant of `lab` that mixes the colors gathered, if not enough colors it fallbacks to usual lab (not recommended in small images)",
          "type": "string",
          "enum": [
            "labmixed"
          ]
        },
        {
          "description": "CIE Lch, you can understand this color space like LAB but with chrome and hue added. Could help when sorting.",
          "type": "string",
          "enum": [
            "lch"
          ]
        },
        {
          "description": "CIE Lch, you can understand this color space like LAB but with chrome and hue added. Could help when sorting.",
          "type": "string",
          "enum": [
            "lchmixed"
          ]
        },
        {
          "description": "Variant of Lch which preserves 8 colors: black, red, green, yellow, blue, magenta, cyan and gray. This works best with 'darkansi' palette, allowing a constant color order.",
          "type": "string",
          "enum": [
            "lchansi"
          ]
        }
      ]
    },
    "FallbackGenerator": {
      "oneOf": [
        {
          "description": "uses [`interpolate`]",
          "type": "string",
          "enum": [
            "interpolate"
          ]
        },
        {
          "description": "uses [`complementary`]",
          "type": "string",
          "enum": [
            "complementary"
          ]
        }
      ]
    },
    "Fields": {
      "description": "An entry within the config file, toml table ref: <https://toml.io/en/v1.0.0#array-of-tables>",
      "type": "object",
      "required": [
        "target",
        "template"
      ],
      "properties": {
        "pywal": {
          "description": "Allows pywal template spec compatibility (disabled by default)",
          "type": [
            "boolean",
            "null"
          ]
        },
        "target": {
          "description": "Where to write the template",
          "type": "string"
        },
        "template": {
          "description": "A file inside `~/.config/wallust/`, which is used for templating",
          "type": "string"
        }
      }
    },
    "Palette": {
      "description": "Corresponds to the modules inside this module and `palette` parameter in the config file.",
      "oneOf": [
        {
          "description": "8 dark colors, dark background and light contrast",
          "type": "string",
          "enum": [
            "dark"
          ]
        },
        {
          "description": "Same as `dark` but uses the 16 colors trick",
          "type": "string",
          "enum": [
            "dark16"
          ]
        },
        {
          "description": "This is a `dark` variant that changes all colors to it's complementary counterpart, giving the feeling of a 'new palette' but that still makes sense with the image provided.",
          "type": "string",
          "enum": [
            "darkcomp"
          ]
        },
        {
          "description": "16 variation of the dark complementary variant",
          "type": "string",
          "enum": [
            "darkcomp16"
          ]
        },
        {
          "description": "This is not a 'dark' variant, is a new palette that is meant to work with `lchansi` colorspace, which will maintain 'tty' like color order and only adjusting the colors acording to the theme. A possible solution for LS_COLORS and the like. Should workout with other colorspace, but the result may not be optimal.",
          "type": "string",
          "enum": [
            "ansidark"
          ]
        },
        {
          "description": "The ansidark palette with 16 color variation.",
          "type": "string",
          "enum": [
            "ansidark16"
          ]
        },
        {
          "description": "Same as `dark` with hard hue colors",
          "type": "string",
          "enum": [
            "harddark"
          ]
        },
        {
          "description": "Harddark with 16 color variation",
          "type": "string",
          "enum": [
            "harddark16"
          ]
        },
        {
          "description": "complementary colors variation of harddark scheme",
          "type": "string",
          "enum": [
            "harddarkcomp"
          ]
        },
        {
          "description": "complementary colors variation of harddark scheme",
          "type": "string",
          "enum": [
            "harddarkcomp16"
          ]
        },
        {
          "description": "Light bg, dark fg",
          "type": "string",
          "enum": [
            "light"
          ]
        },
        {
          "description": "Same as `light` but uses the 16 color trick",
          "type": "string",
          "enum": [
            "light16"
          ]
        },
        {
          "description": "complementary colors variation of light",
          "type": "string",
          "enum": [
            "lightcomp"
          ]
        },
        {
          "description": "complementary colors variation of light with the 16 color variation",
          "type": "string",
          "enum": [
            "lightcomp16"
          ]
        },
        {
          "description": "Variant of softlight, uses the lightest colors and a dark background (could be interpreted as `dark` inversed)",
          "type": "string",
          "enum": [
            "softdark"
          ]
        },
        {
          "description": "softdark with 16 color variation",
          "type": "string",
          "enum": [
            "softdark16"
          ]
        },
        {
          "description": "complementary variation for softdark",
          "type": "string",
          "enum": [
            "softdarkcomp"
          ]
        },
        {
          "description": "complementary variation for softdark with the 16 color variation",
          "type": "string",
          "enum": [
            "softdarkcomp16"
          ]
        },
        {
          "description": "Light with soft pastel colors, counterpart of `harddark`",
          "type": "string",
          "enum": [
            "softlight"
          ]
        },
        {
          "description": "softlight with 16 color variation",
          "type": "string",
          "enum": [
            "softlight16"
          ]
        },
        {
          "description": "softlight with complementary colors",
          "type": "string",
          "enum": [
            "softlightcomp"
          ]
        },
        {
          "description": "softlight with complementary colors with 16 colors",
          "type": "string",
          "enum": [
            "softlightcomp16"
          ]
        }
      ]
    }
  }
}
