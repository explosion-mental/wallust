# Tests

The major testing occurs with the `pywal template engine` in `template-pywal`.
Definitions of colors and other variables are inside `template/mod.rs`


The file `template-pywal`, test against the [original pywal templates](https://github.com/dylanaraps/pywal/tree/master/pywal/templates),
to avoid breaking the engine, constantly testing is needed.
