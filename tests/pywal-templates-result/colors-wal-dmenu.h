static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#0F0000", "#000000" },
	[SchemeSel] = { "#0F0000", "#010000" },
	[SchemeOut] = { "#0F0000", "#0E0000" },
};
