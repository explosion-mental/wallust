module.exports.colors = {
  dark: {
    accent0: "#000000",
    accent1: "#010000",
    accent2: "#020000",
    accent3: "#030000",
    accent4: "#040000",
    accent5: "#050000",
    accent6: "#DDDDDD",
    accent7: "#070000",
    shade0:  "#EEEEEE",
    shade1:  "#090000",
    shade2:  "#0A0000",
    shade3:  "#0B0000",
    shade4:  "#0C0000",
    shade5:  "#0D0000",
    shade6:  "#0E0000",
    shade7:  "#DDDDDD"
  },
};
