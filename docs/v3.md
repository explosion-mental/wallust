# Migration guide to v3
_The last stable backwards compatible version is 2.10._ V3 builds have the following breaking changes:

## Cli
The usual, and shorter, `wallust image.png`, is deprecated.
Instead, you should use `wallust run image.png`.

Regarding cli flags: `--filter` and `-f` will be replaced by `--palette` and `-p`, respectively.

## wallust.toml
**You can update your configuration file syntax with:** `wallust migrate`

Old syntax:
```toml
[[entry]]
template = "zathurarc"
target = "~/.config/zathura/zathurarc"

[[entry]]
template = "dunst"
target = "~/.config/dunst/dunstrc"
...
```

New syntax:
```toml
[templates]
# usual dotted fields
dunst.template = 'dunstrc.monitor'
dunst.target = '~/.config/dunst/dunstrc'

# or inline
zathura = { template = 'zathura', target = '~/.config/zathura/zathurarc' }

# templated alias to src and target to dst
res.src = 'xres'
res.dst = '~/.config/Xresources'
res.pywal = true #enable pywal syntax

# inline with aliases, even more shorter!
glava = { src = 'glava/main.glsl', dst = '~/.config/glava/rc.glsl' }

# inline with pywal
test = { template = 'test.json', dst = '~/.config/complicated.json', pywal = true }

# directory usecase, all my pywal templates go here.
dir.template = "templates/"
dir.target = "~/.cache/wal/"
dir.pywal = true
```
This is a more shorter and simpler way. Inside the `templates` header, you can
use the `template` and `target` attributes, also alias as `src` and `dst`
respectively.

You can now declare **directories** as templates and targets **non-recursively**.

### Deprecation of `new_engine`

The `new_engine` optional value has been stablished as the default engine and
**is no longer a valid variable**. Now, if you want to enable pywal like syntax
(meaning it's disabled by default), you do so by declaring `pywal = true`.

### Rename `filter` -> `palette`

Just like with the [cli](#cli), filter to palette rename has also affected the config:

Old syntax:
```toml
filter = dark16
```

New syntax:
```toml
palette = dark16
```

## Templates

Following the name changes, variable name changes also have been applied:
`{{filter}}` -> `{{palette}}`.
Or, if `pywal` enabled, `{filter}` -> `{palette}`.


Variables has been reduced to the ones with no `.method` syntax like. This is
because there has been included a runtime that can interpret filters. For example:
```
{{color0.rgb}}
```
is now written like:
```
{{color0 | rgb}}
```

In these lines, escaping `{{` and `}}` has also changed, being avaliable literals as variables:
```
{{{{}}{{}}}}
```
is now written like:
```
{{ "{{}}" }}
```

To learn more about the new template syntax please read the man page `man wallust`.
