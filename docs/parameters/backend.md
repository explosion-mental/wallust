# Backend

Allows you to choose which method to use in order to parse the image.

{{#include ./backend-table.md}}

<hr>

To edit this value:
- **Config file**: `backend = "full"`
- **Cli**: `wallust run image.png --backend full`
