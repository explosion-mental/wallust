# Check Constrast

Ensures a "readable contrast".
Should only be enabled when you notice an unreadable contrast frequently
happening with your images. The reference color for the contrast is the
background color. (default: **disabled**)

<hr>

To edit this value:
- **Config file**: `check_contrast = true`
- **Cli**: `wallust run image.png --check-contrast`
