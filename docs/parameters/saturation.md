# Saturation
Color saturation, usually something higher than 50 increases the saturation and
below decreases it (on a scheme with strong and vivid colors).

_Possible values:_ 1 - 100 (default: **disabled**)

<hr>

To edit this value:
- **Config file**: `saturate = 20`
- **Cli**: `wallust run image.png --saturation 20`
