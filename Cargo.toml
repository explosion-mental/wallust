[package]
name = "wallust"
version = "3.2.0"
edition = "2021"
license = "MIT"
authors = [ "explosion-mental" ]
keywords = [ "cli", "template", "colorscheme", "theme" ]
categories = [ "command-line-utilities" ]
description = "Generate a 16 color scheme based on an image."
repository = "https://codeberg.org/explosion-mental/wallust"
homepage = "https://explosion-mental.codeberg.page/wallust"

# See more keys and their definitions at https://doc.rust-lang.org/cargo/reference/manifest.html

[dependencies]
thiserror = "2.0"
anyhow = "1.0"
image = "0.25"
owo-colors = "4.1"
toml = "0.8"
toml_edit = "0.22"
serde_json = "1.0"
shellexpand = "3.1"
glob = "0.3"
spinners = "4.1"
dirs = "6.0"
itertools = "0.14"
fast_image_resize = "5.1"
num-traits = "0.2"
kmeans_colors = { version = "0.6.0", default-features = false, features = ["palette_color"] }
palette = { version = "0.7", default-features = false }

# replacement for fs::canonalize but handles way better windows paths
dunce = "1.0"

clap  = { version = "4.5", features = ["derive"] }
serde = { version = "1.0", features = ["derive"] }
minijinja = { version = "2.7", features = ["loader"] }

# optionals
fastrand = { version = "2.3", optional = true }
wallust_themes = { version = "1.0", optional = true }

# for making the doc tables from comments in source code
documented = { version = "0.9.1", optional = true }
strum = { version = "0.27", features = ["derive"], optional = true }
schemars = { version = "0.8", optional = true }

[dev-dependencies]
tempfile = "3.17"
criterion = { version = "0.5", features = ["html_reports"] }
clap_complete = "4.5"
clap_mangen = "0.2"
dirs = "6.0"

[build-dependencies]
wallust_themes = "1.0"
clap        = { version = "4.5", default-features = false, features = ["std", "cargo"] }
vergen-git2 = { version = "1.0", default-features = false, features = ["cargo", "emit_and_set"] }

[features]
default = [ "themes" ]
# include built-in themes (~130K) <https://codeberg.org/explosion-mental/wallust-themes>
themes = [ "dep:fastrand", "dep:wallust_themes" ]
schema = [ "dep:schemars" ]

# build generated stuff, used for building man pages and completions
buildgen = [ "doc", "iter", "schema", "clap/cargo" ]
doc  = [ "dep:documented" ]
iter = [ "dep:strum" ]

[[bench]]
name = "backends"
harness = false
required-features = ["iter"]

[[bench]]
name = "colorspaces"
harness = false
required-features = ["iter"]

[profile.release]
incremental = false
strip = true
lto   = true # Seems to increase ~0.2 secs in speed..
